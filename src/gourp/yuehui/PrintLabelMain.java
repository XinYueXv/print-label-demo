package gourp.yuehui;

import com.sun.jna.Library;
import com.sun.jna.Native;

public class PrintLabelMain {
    public interface TscLibDll extends Library {
        TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary ("TSCLIB", TscLibDll.class);

        // 以下为dll函数库支持的方法，方法的作用与参数说明见附件【dll函数库api文档】
        int about ();
        int openport (String pirnterName);
        int closeport ();
        int sendcommand (String printerCommand);
        int setup (String width,String height,String speed,String density,String sensor,String vertical,String offset);
        int downloadpcx (String filename,String image_name);
        int barcode (String x,String y,String type,String height,String readable,String rotation,String narrow,String wide,String code);
        int printerfont (String x,String y,String fonttype,String rotation,String xmul,String ymul,String text);
        int clearbuffer ();
        int printlabel (String set, String copy);
        int formfeed ();
        int nobackfeed ();
        int windowsfont (int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName, String content);
    }

    public static void main(String[] args) {
        TscLibDll.INSTANCE.about();

        // 调用打印机的流程如下
        // 1.指定打印机
        TscLibDll.INSTANCE.openport("TSC TTP-2410M");
        // 2.设置打印机纸张规格
        TscLibDll.INSTANCE.setup("100", "100", "5", "8", "0", "0", "0");
        // 3.清除上次打印后的缓存
        TscLibDll.INSTANCE.clearbuffer();
        // 4.将字体写入暂存区准备打印
        TscLibDll.INSTANCE.printerfont ("100", "10", "3", "0", "1", "1", "(JAVA) DLL Test!!");
        // 5.调用打印机打印
        TscLibDll.INSTANCE.printlabel("1", "1");
        // 6.断开和打印机的连接
        TscLibDll.INSTANCE.closeport();



        // TscLibDll.INSTANCE.sendcommand("REM ***** This is a test by JAVA. *****");

        // TscLibDll.INSTANCE.sendcommand("PUTPCX 550,10,\"UL.PCX\"");

        TscLibDll.INSTANCE.barcode("100", "40", "128", "50", "1", "0", "2", "2", "123456789");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 0, 3, 1, "arial", "DEG 0");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 90, 3, 1, "arial", "DEG 90");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 180, 3, 1, "arial", "DEG 180");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 270, 3, 1, "arial", "DEG 270");

    }

}